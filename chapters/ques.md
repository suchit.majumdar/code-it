---
title: ''
description:
  ""
prev: null
next: null
type: chapter
id: 20
---
<exercise id="2" title="Strings to hashes">

### Ques 1

- Print 'Hello World' as output
- Incase you are not sure check the solution, reset and then try again

<codeblock id="02_02_01">

- You can use the string store in `nlp.vocab.strings` like a regular Python
  dictionary. For example, `nlp.vocab.strings['unicorn']` will return the hash,
  and looking up the hash again will return the string `'unicorn'`.

</codeblock>

</exercise>

<br/><br/><br/>

<exercise id="2" title="Strings to hashes">

### Ques 2

- Print 'Hello World' as output
- Incase you are not sure check the solution, reset and then try again

<codeblock id="02_02_01">

- You can use the string store in `nlp.vocab.strings` like a regular Python
  dictionary. For example, `nlp.vocab.strings['unicorn']` will return the hash,
  and looking up the hash again will return the string `'unicorn'`.

</codeblock>

</exercise>
